# Sleep ADL Tracking poster

```
pdflatex main.tex
bibtex main.aux
pdflatex main.tex
```

Should take care of everything.

## Stable releases

Look in the
[Tags](https://gitlab.inria.fr/yelkhadi/Sleep-adl-tracking-poster/tags) for
direct PDF access.
